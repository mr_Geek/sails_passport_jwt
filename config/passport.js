var passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    jwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt ? require('passport-jwt').ExtractJwt : null,
    bycrypt = require('bcryptjs');

var EXPIRES_IN_MINUTES = 60 * 24
var SECRET = process.env.tokenSecret || '4ukI0uIVnB3iI1yxj646fVXSE3ZVk4doZgz6fTbNg7jO41EAtl20J5F7Trtwe7OM'
var ALGORITHM = 'HS256'
var ISSUER = 'app.com'
var AUDIENCE = 'app.com'

var LOCAL_STRATEGY_CONFIG = {
  usernameField: 'username',
  passwordField: 'password',
  passReqToCallback: false
}

var jwtFromRequest = ExtractJwt ? ExtractJwt.fromAuthHeader() : null;
var JWT_STRATEGY_CONFIG = {
  jwtFromRequest: jwtFromRequest,
  secretOrKey: SECRET,
  issuer: ISSUER,
  audience: AUDIENCE,
  passReqToCallback: false
}

function _onLocalStrategyAuth(username, password, next) {
  User.find({username:username}).exec(function(err, user) {
    if (err) {
      return next(err, false, {});
    }
    if (!user || user.length < 1) {
      return next(null, false, {
        code: 'E_USER_NOT_FOUND',
        message: username + ' is not found'
      });
    }
    bycrypt.compare(password, user[0].password, function(err, res) {
      if (err || !res) {
        Manager.findOne({username: username}, function(err, manager) {
          if(err) {
            return next(err, false, {})
          }
          if (!manager) {
            return next(null, false, {
              code: 'E_MANAGER_NOT_FOUND',
              message: username + ' is not found'
            })
          }
          bycrypt.compare(password, manager.password, function(errManager, resManager) {
            if (errManager || !resManager) {
              return next(null, false, {
                code: 'E_WRONG_PASSWORD',
                message: 'Password is wrong'
              });
            } else {
              return next(null,manager, {});
            }
          })
        })
      } else {
        return next(null,user, {});
      }
    });
  });
}

function _onJwtStrategyAuth(payload, next) {
  var user = payload.user
  return next(null, user, {})
}

passport.use(new LocalStrategy(LOCAL_STRATEGY_CONFIG, _onLocalStrategyAuth))
passport.use(new jwtStrategy(JWT_STRATEGY_CONFIG, _onJwtStrategyAuth))

module.exports.jwtSettings = {
  expiresIn: EXPIRES_IN_MINUTES,
  secret: SECRET,
  algorithm : ALGORITHM,
  issuer : ISSUER,
  audience : AUDIENCE
};
