/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

var bycrypt = require('bcryptjs');

module.exports = {

  adapter: 'mongo',

  schema: true,

  attributes: {

    name: {
      type: 'string'
    },

  	username: {
  		type: 'string',
      unique: true
  	},

  	password: {
  		type: 'string'
  	},

    id_num: {
      type: 'string'
    },

    phone: {
      type: 'string'
    },

    address: {
      type: 'string'
    },

    lng: {
      type: 'string'
    },

    lat: {
      type: 'string'
    },

    email: {
      type: 'string',
      defaultsTo: ''
    },

    toJSON: function(){
      var obj = this.toObject()
      delete obj.password
      return obj
    }
  },


  beforeCreate: function(user, cb){
     console.log("- User.beforeCreate : creating hash and salt for password");
     bycrypt.genSalt(10, function(err, salt){
       if(err) {
         console.log('Error generating SALT for user: ' + user + '\n Error: ' + err);
         cb(err);
       }
       bycrypt.hash(user.password, salt, function(err, hash){
         if (err){
           console.log('Error generating HASH for user: ' + user + '\n Error: ' + err);
           cb(err);
         }
         else{
           user.password = hash;
           console.log(hash);
           cb(null, user);
         }
       });
     });
   }

};
