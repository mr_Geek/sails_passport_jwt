var jwt = require('jsonwebtoken')

module.exports = {
  secret: sails.config.jwtSettings.secret,
  issuer: sails.config.jwtSettings.issuer,
  audience: sails.config.jwtSettings.audience,

  createToken: function(user) {
    return jwt.sign({
      user: user.toJSON()
    },
    sails.config.jwtSettings.secret,
    {
      algorithm: sails.config.jwtSettings.algorithm,
      expiresIn: sails.config.jwtSettings.expiresInMinutes,
      issuer: sails.config.jwtSettings.issuer,
      audience: sails.config.jwtSettings.audience
    })
  }
}
