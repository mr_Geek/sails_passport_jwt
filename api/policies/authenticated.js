var passport = require('passport')


module.exports = function (req, res, next) {
  passport.authenticate('jwt', function (err, user, info) {
    if (err) return res.serverError(err)
    if (!user) {
      return res.unauthorized(null, info && info.code, info && info.message)
    }
    req.user = user
    next()
  })(req, res)
}

// module.exports = function(req, res, next) {
//   // User is allowed, proceed to the next policy,
//   // or if this is the last policy, the controller
//   // Sockets
//   if(req.isSocket)
//   {
//     if(req.session &&
//       req.session.passport &&
//       req.session.passport.user)
//     {
//       //Use this:
//       passport.authenticate('jwt', function (err, user, info) {
//         if (err) return res.serverError(err)
//         if (!user) {
//           return res.unauthorized(null, info && info.code, info && info.message)
//         }
//         req.user = user
//         sails.config.passport.initialize()(req, res, function () {
//           // Use the built-in sessions
//           sails.config.passport.session()(req, res, function () {
//             // Make the user available throughout the frontend
//             //res.locals.user = req.user;
//             //the user should be deserialized by passport now;\
//             next()
//           });
//         });
//       })(req, res)
//     }
//     else {
//       res.json(401);
//     }
//   }
//   else if (req.isAuthenticated() || (req.session.me && req.session.user) ) {
//     passport.authenticate('jwt', function (err, user, info) {
//       if (err) return res.serverError(err)
//       if (!user) {
//         return res.unauthorized(null, info && info.code, info && info.message)
//       }
//       console.log(req.user);
//       req.user = user
//       console.log(req.user);
//       next()
//     })(req, res)
//   }
//   else{
//     // User is not allowed
//     // (default res.forbidden() behavior can be overridden in `config/403.js`)
//     return res.redirect('/login');
//   }
// };
